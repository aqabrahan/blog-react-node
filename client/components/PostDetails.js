'use strict';
import React, { Component, PropTypes } from 'react';

class PostDetails extends Component {
    static contextTypes = {
        router: PropTypes.object
    };
    componentDidMount() {
        this.props.fetchPost(this.props.postId);
    }

    render() {
        const { post, loading, error } = this.props.activePost;
        if (loading) {
            return <div className="container">Loading...</div>;
        } else if(error) {
            return  <div className="alert alert-danger">{error.message}</div>
        } else if(!post) {
            return <span />
        }
        return (
            <div className="container">
                <h3>{post.title}</h3>
                <h6>contact: {post.contactName}</h6>
                <p>{post.description}</p>
          </div>
        );
    }
}

export default PostDetails;