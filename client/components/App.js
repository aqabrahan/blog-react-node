'use strict';

import React, { Component } from 'react';

class App extends Component {
    render() {
        return (
            <div className="appcomponent">
                {this.props.children}
            </div>
        );
    }
}

module.exports = App;