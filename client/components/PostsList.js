'use strict';

import React, { Component } from 'react';
import { Link } from 'react-router';

class PostsList extends Component {
  componentWillMount() {
    console.log('componenent');
    this.props.fetchPostsC();
  }

  renderPosts(posts) {
    console.log(posts);
    return posts.data.map((post) => {
      return (
        <li className="list-group-item" key={post.id}>
          <Link style={{color:'black'}} to={"posts/" + post.id}>
            <h3 className="list-group-item-heading">{post.title}</h3>
          </Link>
        </li>
      );
    });
  }

  render() {
    const { posts, loading, error } = this.props.postsList;
    if(loading) {
      return <div className="container"><h1>Posts</h1><h3>Loading...</h3></div>
    } else if(error) {
      return <div className="alert alert-danger">Error: {error.message}</div>
    }
    return (
      <div className="container">
        <h1>Posts</h1>
        <ul className="list-group">
          {this.renderPosts(posts)}
        </ul>
      </div>
    );
  }
}


export default PostsList;
