'use strict';

import {
    FETCH_POSTS,
    FETCH_POSTS_SUCCESS,
    FETCH_POSTS_FAILURE,
    FETCH_POST,
    FETCH_POST_SUCCESS,
    FETCH_POST_FAILURE
} from '../actions/posts';

const INITIAL_STATE = {
    postsList: {
        posts: [],
        error: null,
        loading: true
    },
    newPost: {
        post: null,
        error: null,
        loading: false
    },
    activePost: {
        post: null,
        error: null,
        loading: false
    },
    deletedPost: {
        post: null,
        error: null,
        loading: false
    }
};
function reducer(state = INITIAL_STATE, action) {
    let error;
    console.log('llega a reducer' + action.type) ;
    switch (action.type) {
        case FETCH_POSTS:
            console.log('get posts reducer ' + FETCH_POSTS);

            return {
                ...state,
                postsList: {
                    posts: [],
                    error: null,
                    loading: true
                }
            };
        case FETCH_POSTS_SUCCESS:
            console.log('get posts reducer sucess');
            return {
                ...state,
                postsList: {
                    posts: action.payload,
                    error: null,
                    loading: false
                }
            };
        case FETCH_POSTS_FAILURE:
            error = action.payload || {message: action.payload.message};
            return {
                ...state,
                postsList: {
                    posts: [],
                    error: error,
                    loading: false
                }
            };
        case FETCH_POST:
                return {
                    ...state,
                    activePost: {
                        ...state.activePost,
                        loading: true
                    }
                };
        case FETCH_POST_SUCCESS:
                return {
                    ...state,
                    activePost: {
                        post: action.payload,
                        error: null,
                        loading: false
                    }
                };
        case FETCH_POST_FAILURE:
                error = action.payload || {message: action.payload.message};//2nd one is network or server down errors
                return {
                    ...state,
                    activePost: {
                        post: null,
                        error:error,
                        loading: false
                    }
                };
            /*case RESET_POSTS:// reset postList to initial state
              return { ...state, postsList: {posts: [], error:null, loading: false} };

            case FETCH_POST:
              return { ...state, activePost:{...state.activePost, loading: true}};
            case FETCH_POST_SUCCESS:
              return { ...state, activePost: {post: action.payload, error:null, loading: false}};
            case FETCH_POST_FAILURE:
              error = action.payload || {message: action.payload.message};//2nd one is network or server down errors
              return { ...state, activePost: {post: null, error:error, loading:false}};
            case RESET_ACTIVE_POST:
              return { ...state, activePost: {post: null, error:null, loading: false}};*/
        default:
            return state;
    }
}

export default reducer;