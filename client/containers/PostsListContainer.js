'use strict';

import { connect } from 'react-redux'
import { fetchPosts, fetchPostsSuccess, fetchPostsFailure } from '../actions/posts';
import PostsList from '../components/PostsList';
const mapStateToProps = (state) => {
  console.log('state container');
  console.log(state);
  return {
    postsList: state.posts.postsList
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchPostsC: () => {
      let a = fetchPosts();
      console.log('a');
      console.log(a);
      dispatch(a).then((response) => {
          console.log('response in container');
          console.log(response);
            !response.error ? dispatch(fetchPostsSuccess(response.payload.data)) : dispatch(fetchPostsFailure(response.payload.data));
          });
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostsList);