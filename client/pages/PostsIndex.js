'use strict';

import React, { Component } from 'react';
//import HeaderContainer from '../containers/HeaderContainer';
import PostsList from '../containers/PostsListContainer';
//<HeaderContainer type="posts_index" />
class PostsIndex extends Component {
    render() {
        return (
            <div className="pageindex">
                <PostsList />
            </div>
        );
    }
}

module.exports = PostsIndex;