'use strict';
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import PostDetailsContainer from '../containers/PostsDetailsContainer.js';

class PostShow extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    render() {
        return (
            <div className="container">
                <PostDetailsContainer id={this.props.params.id} />
            </div>
        );
    }
}

export default PostShow;