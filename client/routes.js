'use strict';

import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './pages/App';
import PostsIndex from './pages/PostsIndex';
import PostsShow from './pages/PostsShow';
/*<Route path="posts/new" component={PostsNew} />
        <Route path="posts/:id" component={PostsShow} />
        <Route path="/signin" component={SignIn} />
        <Route path="/signup" component={SignUp} />
        <Route path="/forgotpwd" component={ForgotPwd} />
        <Route path="/validateEmail/:token" component={ValidateEmail} />
        <Route path="/profile" component={Profile} />*/
export default (
    <Route path="/" component={App}>
        <IndexRoute component={PostsIndex}/>
        <Route path="posts/:id" component={PostsShow} />
    </Route>
);