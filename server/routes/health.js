'use strict';

//let _ = require('underscore');
//let asynquence = require('asynquence');
let Endpoint = require('../modules/endpoint');
//let winston = require('../modules/winston');


/**
 *  @swagger
 *  /api/health:
 *    get:
 *      responses:
 *        200:
 *          description: Devuelve un health check de sus servicios
 */
class Health extends Endpoint {

    registerRoutes() {
        this.api.get('/health', this.handlerGet.bind(this));
    }

    handlerGet(req, res, next) {
        console.log('arrigve');
        this.send(res, req, {response: { statusCode:200 }, data: {a:300}});
    }
}

module.exports = Health;