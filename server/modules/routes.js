'use strict';

let _ = require('underscore');
let asynquence = require('asynquence');
let fs = require('fs');
let winston = require('winston');

class Routes {
    constructor(api) {
        this.api = api;
        this.route = [];

        asynquence()
            .then(this.readFiles.bind(this))
            .val(this.registerRoutes.bind(this));
    }

    readFiles(callback) {
        this.dir = `${process.cwd()}/server/routes/`; //todo: pasasr a config

        fs.readdirSync(this.dir)
            .forEach((file) => {
                this.route.push(`${this.dir}/${file}`);
            });
        callback();
    }

    registerRoutes(callback) {
        _.each(this.route, (value) => {
            let Route = require(value);
            return Route.isEnabled() ? new Route(this.api) : false;
        })
    }

    // methods
}

module.exports = Routes;