'use strict';
let _ = require('underscore');
let cluster = require('cluster');
let config = require('../config');
let winston = require('winston');

const COUNT = require('os').cpus().length || config.app.cluster.workers || 1;

class Cluster {
    constructor(count) {
        console.log(COUNT);
        console.log('arrive');
        this.count = count || COUNT;
    }

    start() {
        if(!cluster.isMaster) {
            return cluster.worker;
        }

        _.times(this.count, () => {
            cluster.fork();
        });
        cluster.on('exit', worker => {
            winston.info(`id:${worker.id} pid:${process.id} exiting`);
            cluster.fork();
        });
    }

}

module.exports = Cluster;