'use strict';

let _ = require('underscore');
let asynquence = require('asynquence');
let config = require('../config');
//let services = require('../services');
let time = require('../helpers/time');
let util = require('util');
let winston = require('./winston');

class Endpoint {
    constructor(api) {
        this.api = api;
        this.config = config;
        this.defaultCache = '30 minutes';
        //this.services = services;
        this.errors = [];
        this.registerRoutes();
    }

    static isEnabled() {
        return true;
    }

    name() {}

    registerRoutes() {}

    validate() {}

    handler() {}

    cache(duration) {
        duration = duration || this.defaultCache;

        let toTime = time.toMilliseconds(duration);
        let mtime = new Date();

        return (req, res, next) => {
            if (!res.getHeader('Cache-Control')) {
                res.setHeader('Cache-Control','public, max-age=' + toTime);
                res.setHeader('Expires', new Date(Date.now() + toTime).toUTCString());
                res.setHeader('Last-Modified', mtime.toUTCString());
            }
            next();
        };
    }

    error(res, err) {
        if (!_.isEmpty(this.errors)) {
            err = this.errors;
        }
        winston.error(`Error: status: ${err.response.statusCode}`);

        _.each(this.errors, (v) => {
            winston.error(`message: ${v.code} -> ${v.text}`);
        });

        this.response(res, {
            response: {
                statusCode: err.response.statusCode || 500
            },
            data: {
                errors: err.errors
            }
        });
    }

    send(res, req, result) {
        console.log(result);
        if (!_.isEmpty(result.errors) || result.response.statusCode in [500, 503, 404, 400]) {
            return this.error(res, result);
        }

        if (_.isString(result.data) && !_.isEmpty(result.data)) {
            try {
                result.data = result.data
            }catch(e) {
                winston.error('Catch error: ' + e);
                result.data = {
                    message: ' No readable error: ' + e,
                    error: e
                };
                return this.error(res, result);
            }
        }

        if (result.response.statusCode == 204) {
            result.response.statusCode = 200;
        }

        if (!!req && !!result.data && !_.isEmpty(result.data.sessionId)) {
            req.session.sessionId = result.data.sessionId;
            req.session.search = {
                response: {
                    statusCode: 200
                },
                data: {
                    response: result.data.response,
                    sessionId: result.data.sessionId
                }
            };
        }

        return this.response(res, result);

    }

    response(res, result) {
        res.status(result.response.statusCode).json(result.data);
    }
}

module.exports = Endpoint;