'use strict';
let _ = require('underscore');
let config = require('../config');
let winston = require('winston');

const R_N = /\n$/g;
const TRANSPORTS = config.winston.transports;
const OPTIONS = config.winston.options || {};
const COLORS = config.winston.colors.levels || winston.config.syslog.colors;

class Winston {
    constructor() {
        this.loggers = {};
        _.each(TRANSPORTS, (transport, name) => {
            this.loggers[name] = new winston.Logger({
                levels: transport.levels,
                colors: COLORS,
                transports: [new winston.transports[transport.type || 'Console'](_.defaults(transport.options, OPTIONS))]
            });
            for (let level in this.loggers[name].levels) {
                this[level] = this._log.bind(this, name, level);
            }
            this.setStreams(name, transport.levels);
        }, this);
    }

    _log(name, level, message) {
        this.loggers[name][level](message);
    }

    setStreams(name, levels) {
        this.streams = {};
        for (let level in levels) {
            this.setStream(name, level);
        }
    }

    setStream(name, level) {
        this[`${level}Stream`] = {
            write: message => {
                this.loggers[name][level](message.replace(R_N, ' '));
            }
        };
    }
}

module.exports = new Winston();