'use strict';

let _ = require('underscore');
let asynquence = require('asynquence');
let bodyParser = require('body-parser');
let Cluster = require('./modules/cluster');
let config = require('./config');
let cookieParser = require('cookie-parser');
let express = require('express');
let expressSession = require('express-session');
let favicon = require('serve-favicon');
let helmet = require('helmet');
let MongoStore = require('connect-mongo')(expressSession);
let morgan = require('morgan');
let Router = require('./router');
//let Swagger = require('./modules/swagger');
let winston = require('./modules/winston');

//var compression = require('compression');

class Server {
    constructor(args) {
        this.api = express.Router();
        this.app = express();
        this.port = config.app.port;
    }

    start() {
        let steps = asynquence();

        steps
            .then(this.configure.bind(this))
            .then(this.routing.bind(this))
            .then(this.swaggerize.bind(this));

        if(config.app.cluster.enabled) {
            steps.then(this.clusterize.bind(this));
        }

        steps.val(this.init.bind(this));
    }

    configure(callback) {
        let errors = [];
        asynquence()
            .then((done) => {
                this.app.disabled('x-powered-by');
                //this.app.use(favicon(`${process.cwd()}/public/assets/images/favicon.ico`));
                this.app.use(express.static(`public`));
                this.app.use('/', express.static(`${process.cwd()}/public`));
                this.app.use('/api', this.api);
                this.app.use(this.redirectUnmatched);
                this.api.use((req, res, next) => {
                    res.header('Access-Control-Allow-Origin', '*');
                    res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
                    next();
                });
                this.api.use(morgan(config.morgan.mode));
                this.api.use(cookieParser());
                this.api.use(bodyParser.urlencoded({extended: true}));
                this.api.use(expressSession({
                    secret: config.app.secret,
                    resave: true,
                    saveUninitialized: true,
                    store: new MongoStore({
                        url: config.mongodb.host,
                        ttl: config.session.ttl
                    }),
                    name: 'zd.sid'
                }));
                this.api.use(helmet());

                done();
            })
            .val(() => {
                if (_.isEmpty(errors)) {
                    callback();
                }
            });
    }

    redirectUnmatched(req, res) {
        res.redirect('/');
    }

    routing(callback) {
        this.router = new Router(this.api);

        callback();
    }

    clusterize(callback) {
        this.cluster = new Cluster();
        this.worker = this.cluster.start();

        if (this.worker) {
            this.instances = {
                id: this.worker.id,
                process: process.pid
            }
        }
        callback();
    }

    swaggerize(callback) {
        //this.swagger = new Swa
        callback();
    }

    init() {
        this.app.listen(this.port, () => {
            let msg = this.instances ? `Server run! port: ${this.port} instanc: ${this.instances.id} process: ${this.instances.process}` : 'Run Server!';
            winston.info(msg);
        });
    }
}


module.exports = Server;