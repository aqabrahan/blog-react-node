'use strict';
module.exports = {
    colors: {
        enabled: true,
        levels: {
            emerg: 'red',
            alert: 'yellow',
            crit: 'red',
            error: 'red',
            warning: 'red',
            notice: 'yellow',
            access: 'magenta',
            info: 'green',
            debug: 'cyan'
        }
    },
    options: {
        level: undefined,
        silent: false,
        colorize: true,
        timestamp: false,
        json: false,
        stringify: false,
        prettyPrint: true,
        depth: null,
        humanReadableUnhandledException: true,
        showLevel: true,
        formatter: undefined,
        debugStdout: true
    },
    transports: {
        jarvis: {
            type: 'Console',
            levels: {
                emerg: 1,
                alert: 2,
                crit: 3,
                error: 4,
                warning: 5,
                notice: 6,
                access: 7,
                info: 8,
                debug: 9
            },
            options: {
                level: 'debug'
            }
        }
    }
};