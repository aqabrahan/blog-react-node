'use strict';

module.exports = (mongo) => {
    return {
        host: mongo.host,
        port: mongo.base
    }
}