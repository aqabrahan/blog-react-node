'use strict';
let parameters = require('./parameters.json');

module.exports = {
    app: require('./app')(parameters.app),
    mongodb: require('./mongodb')(parameters.mongo),
    session: require('./session'),
    morgan: require('./morgan'),
    winston: require('./winston')
};