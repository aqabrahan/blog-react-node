'use strict';

let path = require('path');

module.exports = {
  context: path.join(__dirname, "/client"),
  devtool: 'cheap-module-source-map',
  entry: [
    './index.js'
  ],
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loaders: ['react-hot']
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'react', 'stage-0']
        }
      }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
    "alias": {
      "react": "preact-compat",
      "react-dom": "preact-compat"
    }
  },
  output: {
    path: __dirname + '/public/',
    publicPath: '/',
    filename: '/static/[name].min.js'
  },
  //devtool: "eval",
};
